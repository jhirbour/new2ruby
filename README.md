# Welcome
This project is dedicated to helping you, the new and aspiring Rubyist, to learn
the only language in the world designed for developer happiness.

We're just getting started! Founded in mid-July 2014, we're still working on
achieving critical mass. Please bear with us as we get organized.

## Participating

* Join us on IRC! We're on ##new2ruby on freenode.
* Add a resource! Add a link to the [Resources page](https://gitlab.com/coraline/new2ruby/blob/master/Resources.md),
then submit a merge request.

## Code of Conduct

Please note that this project is released with a
[Code of Conduct](https://gitlab.com/coraline/new2ruby/blob/master/CODE_OF_CONDUCT.md).
By participating in this project you agree to abide by its terms.

## Contributing

1. Fork the project
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request